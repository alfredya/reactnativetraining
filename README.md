# React Native Training Project

This is a react native training project based on the react-native-cli.

## Installation

Before proceeding make sure Node.js, JDK 8 and Python 2 are installed.

Install React Native CLI:

`npm install -g react-native-cli`

Clone repository to local project directory and create a development branch

`npm install`

## Run development environment

### iOS

Enter the following command in the project folder to run the app in the XCode iOS simulator

`react-native run-ios`

The Xcode project file is located at /ios/reactNativeSeed.xcodeproj.  Open the project in Xcode and make sure Devices is set to `Universal` under Deployment Info.  Run the Xcode simulator in iPad mode as that will be the primary iOS device.

### Android

With a Android Virtual Device running, enter the following command in the project folder to run the App:

`react-native run-android`


## Hot Reloading

### iOS

In the simulator, press command-D to show developer options.  Enable hot reloading.

### Android

In the virtual device, press command-m (or ctrl-m in Windows) to show developer options.  Enable hot reloading.






